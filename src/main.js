import dotenv from 'dotenv'
dotenv.config()


import Vue from 'vue'
import App from './App.vue'
// Incluimos la instancia de Vues
import store from "./store/index.js";
// Incuimos la instancia de Vue-router
import router from "./router";

import "./plugins/aws/index"
import "./plugins/api/index"

Vue.config.productionTip = false



// Inicializamos el STATE (vuex) de la aplicación

store.dispatch("init").then(()=> {
  
  // Es necesario validar si existe una sesión 
  // sessión activa. 
  // Es necesario ejecutar llamada utilizando
  // la instrucción "await"
  
  
  // Una vez que se han inicializado las propiedades necesarias
  // para la aplicación, se crea la instancia de Vue
  new Vue({
    el: "#app",
    store,
    router,
    render: (h) => h(App),
    created: () => {},
  });
});

  

