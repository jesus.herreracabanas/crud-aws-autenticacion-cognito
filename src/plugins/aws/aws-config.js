export default {
    Auth: {
        // REQUIRED - Amazon Cognito Region
        region: process.env.VUE_APP_AWS_Region,

        // REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
        identityPoolId: process.env.VUE_APP_AWS_identityPoolId,
    
        // OPTIONAL - Amazon Cognito Federated Identity Pool Region 
        // Required only if it's different from Amazon Cognito Region
        // identityPoolRegion: 'us-west-2',

        // OPTIONAL - Amazon Cognito User Pool ID
        userPoolId: process.env.VUE_APP_AWS_userPoolId,

        // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
        userPoolWebClientId: process.env.VUE_APP_AWS_userPoolWebClientId,

        // OPTIONAL - Manually set the authentication flow type. Default is 'USER_SRP_AUTH'
        // 
        // authenticationFlowType: 'CUSTOM_AUTH'

    }
}