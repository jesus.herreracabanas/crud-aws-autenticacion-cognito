import Vue from 'vue'
import Amplify, * as AmplifyModules from 'aws-amplify'
import { AmplifyPlugin } from 'aws-amplify-vue'

import awsconfig from './aws-config'

// awsconfig.API.endpoints.forEach((endpoint => {
//     endpoint.custom_header = async () => {
//         return {
//             "Content-Type": "application/json",
//             Authorization: `Bearer ${(await AmplifyModules.Auth.currentSession()).getIdToken().getJwtToken()}`
//         }
//     }
// }))
Amplify.configure(awsconfig)
Vue.use(AmplifyPlugin, AmplifyModules);
