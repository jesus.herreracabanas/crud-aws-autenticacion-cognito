/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Módulo para el manejo de la autenticación y sesión del usuario
 * El OBJETIVO de este módulo es tener un ejemplo de como se implemente y incluye un módulo Vuex
 * dentro del desarrollo.
 * Este módulo debería manejar el STATE para los procesos de autenticación y manejo de la sesión del
 * usuario.
 * Debería implementar las acciónes de Sigin, Signout.
 *
 * @author          Roberto Pérez Guzmán
 * @creationDate    03 de Mayo del 2020
 */

import { Auth } from "aws-amplify";

export default {
  // El acceso a las propiedades del STATE utiliza el modo de
  // aceso por namespace, es decir, que para accerder a la propiedad
  // se debe seguir el path completo desde el STATE principal y los submodulos
  // correspondientes.
  // en este caso "auth/<propiedad>"
  // "auth" es el nombre con el que se registra este módulo en el STATE pricipal.
  // @see store.js
  namespaced: true,
  // Establecemos el modo estricto para el uso del STATE, lo cual
  // por lo que únicamente los GETTERS y MUTATIONS son los únicos que tienen
  // permitido el acceso las propiedades del STATE de la aplicación.
  strict: true,
  // STATE del módulo
  state: {
    /**
     * Información del usuario
     */
    username: null,
    user: null,
    token: null,
    isEmailVerified: false,
    isPhoneNumberVerified: false,
  },
  getters: {
    /**
     * Determina si el usuario ya ha iniciado sesión o no.
     * @returns true, si el usuario ya ha iniciado sesión, false en caso contrario.
     */
    // Si un usuario ya inició sesión la propiedad user del STATE se encuentra inicializada
    // y es diferente de null
    isAuthenticated: (state) => {
      let { token = null } = state;
      return token !== null;
    },
    /**
     * Token
     */
    token: (state) => {
      let { token = null } = state;
      return token;
    },

    /**
     * Username
     */
    username: (state) => {
      let { username = null } = state;
      return username;
    },

    /**
     * Cognito's user session
     */
    user: (state) => {
      let { user = null } = state;
      return user;
    },

    isEmailVerified: (state) => {
      let { isEmailVerified = null } = state;
      return isEmailVerified;
    },

    isPhoneNumberVerified: (state) => {
      let { isPhoneNumberVerified = null } = state;
      return isPhoneNumberVerified;
    },

  },
  mutations: {
    user(state, { user = {} } ) {
      state.user = user;
      let { username = "" } = user
      state.username = username;
    },
    token(state, { token } ) {
      state.token = token;
    },
    isEmailVerified(state, { isEmailVerified } ) {
      state.isEmailVerified = isEmailVerified;
    },
    isPhoneNumberVerified(state, { isPhoneNumberVerified } ) {
      state.isPhoneNumberVerified = isPhoneNumberVerified;
    },
  },
  actions: {
    /**
     * Inicializa la información de la sesión del usuario.
     *
     */
    async init( {dispatch})  {
      return await dispatch('refreshSession')
    },
    async refreshSession({commit}) {
      try {
        
        let user = await Auth.currentAuthenticatedUser();
        let attributes = await Auth.userAttributes(user)
        let isEmailVerified = false
        let isPhoneNumberVerified = false
        attributes.forEach(attribute => {
          if( attribute.Name === "email_verified") {
            isEmailVerified = attribute.Value === "true";
          } else if( attribute.Name === "phone_number_verified") {
            isPhoneNumberVerified = attribute.Value === "true";
          }
        })
        commit('isEmailVerified',{ isEmailVerified } )
        commit('isPhoneNumberVerified',{ isPhoneNumberVerified } )

        // console.log(`isEmailVerified: ${isEmailVerified}`)
        // console.log(`isPhoneNumberVerified: ${isPhoneNumberVerified}`)
        
        let token = (await Auth.currentSession()).getIdToken().getJwtToken()

        commit('user',{ user } )
        commit('token',{ token } )
      }
      catch(error){
        commit('token', { token: null } )
      }
    },
     /**
     * Realiza el proceso de inicio de sesión del usuario
     * @param payload objeto con las propiedades username, password
     *  {
     *    username: <Nombre de usuario para el inicio de sesión>,
     *    password: <Contraseña para el inicio de sesión>
     *  }
     */
    signIn( { dispatch, commit },  { username, password, $router }) {
        return Auth.signIn(username, password)
          .then( async (user) => await dispatch('processSigninResponse', { user, $router }))
          .catch(error => {
            if(error.code === 'PasswordResetRequiredException'){
              commit("user", { user: { username } })
              $router.replace("/forgotPassword")
            } else {
              throw error;
            }
          });
    },
    signOut({dispatch}, { $router }) {
      return Auth.signOut().then( async ()=>{
        return await dispatch('refreshSession')
        .then(() => {
          $router.replace("/signin");
        });
        
      });
  },
  async setPassword({dispatch, commit}, { user, password, $router }) {
    commit("user", {})
    return Auth.completeNewPassword(user, password) .then( async (user)=>{
      // console.log("completeNewPassword")
      // console.log(user)
      commit("user", user)
      return await dispatch('processSigninResponse', { user, $router });
    })
    .catch(error => {
      if(error.code === 'PasswordResetRequiredException'){
        commit("user", { user })
        $router.replace("/forgotPassword")
      } else {
        throw error;
      }
    });
  },
  
  // eslint-disable-next-line no-empty-pattern
  resetPassword({}, { user, pin, password, $router }) {
    return Auth.forgotPasswordSubmit(user.username, pin, password) .then( async ()=>{
      $router.replace("/signin")
    })
    .catch(error => {
        throw error;
    });
  },

  confirmEmail({ dispatch }, { pin, $router }) {
    return Auth.verifyCurrentUserAttributeSubmit('email', pin) .then(async ()=>{
      let user = await Auth.currentAuthenticatedUser()
      return dispatch('processSigninResponse', { user , $router });
    });
  },

  confirmPhoneNumber({ dispatch }, { pin, $router }) {
    return Auth.verifyCurrentUserAttributeSubmit('phone_number', pin) .then(async ()=>{
      let user = await Auth.currentAuthenticatedUser()
      return dispatch('processSigninResponse', { user , $router });
    });
  },

  confirmMFA({ dispatch, commit }, { user, pin, $router }) {
    commit("user", {})
    return Auth.confirmSignIn(
      user,
      pin,  
      'SMS_MFA' // MFA Type e.g. SMS_MFA, SOFTWARE_TOKEN_MFA
    ).then(async ()=>{
      user = await Auth.currentAuthenticatedUser()
      return dispatch('processSigninResponse', { user , $router });
    });
  },

  sendEmailVerificationCode(){
    return Auth.verifyCurrentUserAttribute('email')
  },
  
  sendPhoneNumberVerificationCode(){
    return Auth.verifyCurrentUserAttribute('phone_number')
  },

  async sendMFAVerificationCode(){
    //return Auth.
  },

  async processSigninResponse({ dispatch, commit, getters }, { user, $router }) {
    commit("user", {user})

    if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
      //Nuevo password es requerido
      //Redireccionar a la página para solicitar las nuevas credenciales
      $router.replace("/newPassword")
    } else if (user.challengeName === 'SMS_MFA') {
        // MFA es requerido
        $router.replace("/mfa")
    } else {
      // Inicio de sesión exitoso
      return await dispatch('refreshSession')
      .then(()=> {
        let isEmailVerified = getters.isEmailVerified;
        let isPhoneNumberVerified = getters.isPhoneNumberVerified;
        if(!isPhoneNumberVerified){
            // Se requiere que el usuario confirme su email
            return dispatch('sendPhoneNumberVerificationCode')
            .then(()=> {
                $router.replace("/confirmPhoneNumber") 
            })
        } else
        if(!isEmailVerified){
          // Se requiere que el usuario confirme su email
          return dispatch('sendEmailVerificationCode')
          .then(()=> {
              $router.replace("/confirmEmail") 
          })
      }
        else {
          $router.replace("/")
        }
      })
    
    }
  },
  // eslint-disable-next-line no-empty-pattern
  resendSignUp( {}, { user } ){
    return Auth.resendSignUp(user.username);
  }
},
};
