/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     STATE de la aplicación
 * @author          Roberto Pérez Guzmán
 * @creationDate    30 de septiembre del 2020
 */

// Incluimos el framework de Vue
import Vue from "vue";
// Incluimos el framework de Vuex
import Vuex from "vuex";
// Incluimos el módulo para el manejo de la autenticación y sesión del usuario
import auth from "./auth/index";
// Asociamos Vuex con Vue
Vue.use(Vuex);

// Método genérico para realizar actualizar una propiedad del STATE
const set = (property) => (store, payload) => (store[property] = payload);

// Creamos la instancia de Vuex
const store = new Vuex.Store({
  // Establecemos el modo estricto para el uso del STATE, lo cual
  // por lo que únicamente los GETTERS y MUTATIONS son los únicos que tienen
  // permitido el acceso las propiedades del STATE de la aplicación.
  strict: true,
  // Incluimos los módulos dependientes
  modules: {
    // Se incluye el módulo para el manejo de la autenticación y sesión del usuario
    // NOTA: por el momento es solo un ejemplo ya que la AUTEHNTICACIóN no esta
    // implementada
    auth,
  },
  // STATE de la aplicación. Mantiene el valor de las propiedades (el estado de la aplicación)
  state: {
    /**
     * Bandera para indicar si la aplicación está realizando alguna tarea y
     * se requiere que el usuario espere hasta que ésta termine.
     * Sirve para presentar un loader en la pantalla, el cual está implementado en
     * el componente principal App.vue
     *
     * @see App.vue
     */
    isLoading: false,
  },
  /**
   * GETTERS
   * Los GETTERS nos permiten obtener la información del STATE
   * Debido a que el modo de operación definido es strict=true, las GETTERS
   * son los únicos que pueden acceder al valor de las propiedades del STATE.
   */
  getters: {
    /**
     * Obtiene el valor de la propiedad isLoading.
     * @returns El valor de la propiedad isLoading.
     */
    isLoading: (state) => {
      return state.isLoading;
    },
  },
  /**
   * MUTACIONES
   * Las mutaciones son los métodos que permiten actualizar el STATE
   * de la aplicación.
   * Dado que el modo de operación definido es strict=true, las mutaciones
   * son los únicos métodos que pueden realizar la actualización del valor del STATE.
   */
  mutations: {
     /**
     * Permite actualizar el valor de la propiedad isLoading.
     *
     */
    isLoading: set("isLoading"),
  },
  /**
   * Las acciones permiten ejecutar operaciones asíncronas y realizan
   * la actualización del STATE a trabés de las mutaciones.
   *
   */
  actions: {
    /**
     * Inicializa el STATE. Este método debe ser llamando tan pronto se instancie el
     * componente de Vue, la aplicación principal.
     * @see main.js
     */
    init({ dispatch }) {
      // Inicialización de módulos
      // Puede inicializar otros módulos si no son dependientes de la sesión del usuario
      //
      
      // Inicializamos el módulo de autenticación y manejo de sesión del usuario
      return dispatch("auth/init").then(() => {
        // Inicializar otros módulos, si son dependientes de las inicialización del
        // módulo de autenticación
      });

      
    },

    /**
     * Establece el valor de la bandera isLoading=true
     */
    initLoader({ commit }) {
      // Ejecuta la mutación para establecer el valor de la
      // propiedad isLoading=true
      commit("isLoading", true);
    },
    /**
     * Establece el valor de la bandera isLoading=false
     */
    stopLoader({ commit }) {
      // Ejecuta la mutación para establecer el valor de la
      // propiedad isLoading=false
      commit("isLoading", false);
    },
  },
});

export default store;
