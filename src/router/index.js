/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     En este archivo se especifican las rutas que estarán disponibles en la aplicación.
 *                  Para mayor información ver la documentación de Vue-router.
 *                  https://router.vuejs.org/
 * @author          Roberto Pérez Guzmán
 * @creationDate    30 de Septiembre del 2020
 */

// Incluimos el framework de Vue
import Vue from "vue";
// Incluimos el framework de Vue-router
import VueRouter from "vue-router";
//Incluimos el STATE de la app (Vuex)
import store from "../store/index";


//
// Incluimos los módulos que estarán disponibles 
// en la aplicación
//
// Incluimos la sección de signin
import Signin from "@/views/Signin/Index";
// Incluimos la sección para establecer un nuevo password
import NewPassword from "@/views/NewPassword/Index";
// Incluimos la sección de resetear el password
import ResetPassword from "@/views/ResetPassword/Index";
// Incluimos la sección de recuperar el password
import ForgotPassword from "@/views/ForgotPassword/Index";
// Incluimos la sección de confirmar email
import ConfirmEmail from "@/views/ConfirmEmail/Index";
// Incluimos la sección de confirmar número de teléfono
import ConfirmPhoneNumber from "@/views/ConfirmPhoneNumber/Index";
// Incluimos la sección de signin
import MFA from "@/views/MFA/Index";

// Incluimos la sección de Home
import Home from "@/views/Home/Index";
// Incluimos la sección de ruta no encontrada (error 404)
import NotFound from "@/views/NotFound/Index";

// Asociamos la Vue-router, como un pluging de Vue
Vue.use(VueRouter);

const router = new VueRouter({
    // Establecemos el modo 'history'
    mode: "history",
    // Definimos las rutas
    routes: [
        // NOT FOUND
        // Componente para las rutas solicitadas por el cliente
        // que no existen en la aplicacion. ERROR 404
        {
            component: NotFound,
            path: "/notFound",
            name: "notFound",
        },
        // SIGNIN
        // Componente para realizar el inicio de sesión
        {
            component: Signin,
            path: "/signin",
            name: "signin",
        },
        // CHANGE PASSWORD
        // Componente para realizar 
        // el cambio de password
        {
            component: NewPassword,
            path: "/newPassword",
            name: "newPassword",
        },
        // CONFIRM EMAIL
        // Componente para confirmar el email
        {
            component: ConfirmEmail,
            path: "/confirmEmail",
            name: "confirmEmail",
        },
        // CONFIRM PHONE NUMBER
        // Componente para confirmar el email
        {
            component: ConfirmPhoneNumber,
            path: "/confirmPhoneNumber",
            name: "confirmPhoneNumber",
        },
        // RESET PASSWORD
        // Componente para realizar el reset 
        // de la contraseña del usuario
        {
            component: ResetPassword,
            path: "/resetPassword",
            name: "resetPassword",
        },
        // FORGOT PASSWORD
        // Componente para realizar el proceso 
        // de recuperación de la contraseña del usuario
        {
            component: ForgotPassword,
            path: "/forgotPassword",
            name: "forgotPassword",
        },
        // MFA
        // Componente para realizar el proceso 
        // de doble autenticación
        {
            component: MFA,
            path: "/mfa",
            name: "mfa",
        },
        // HOME
        {
            component: Home,
            path: "/",
            name: "home",
        }
    ],
});

//
// Navigation Guards
//

/**
 * Validamos la ruta
 *
 * Antes de ingresar a la ruta verificamos que el usuario cuente
 * con los permisos necesarios para el módulo en cuestión.
 */
router.beforeEach((to, from, next) => {
    if (to.matched.length === 0) {
        // Si la ruta solicitada no existe, presentamos la pantalla de error
        next("/notFound");
    } else {
        // Obtenemos el status de la sesión del usuario
        let isAuthenticated = store.getters["auth/isAuthenticated"];
        //console.log(`isAuthenticated: ${isAuthenticated}`)
        //let user = store.getters["auth/user"];
        
        if (isAuthenticated) {
            
            if (to.name === "signin") {
                //Si el usuario ya inicio sesión y la ruta indicada es LOGIN
                //redireccionamos a Home, ya que nos es necesario que  el
                //usuario reingrese sus credenciales
                next("/");
            } 
            else {
                // El usuario tiene una sesión valida
                let isEmailVerified = store.getters["auth/isEmailVerified"]
                let isPhoneNumberVerified = store.getters["auth/isPhoneNumberVerified"]
                if(!isPhoneNumberVerified){
                    // Se requiere que el usuario confirme su email
                    if( to.name !== "confirmPhoneNumber") {
                        store.dispatch('auth/sendPhoneNumberVerificationCode')
                            .then(()=> {
                                next("/confirmPhoneNumber") 
                            })
                        }
                        else {
                            next();
                        }
                }
                else 
                if(!isEmailVerified){
                    // Se requiere que el usuario confirme su email
                    if( to.name !== "confirmEmail") {
                        store.dispatch('auth/sendEmailVerificationCode')
                            .then(()=> {
                                next("/confirmEmail") 
                            })
                        }
                        else {
                            next();
                        }
                }
                else {
                    next() 
                }
            }
        } else {
            // El usuario no tiene una sesión valida
            let { meta = { isAuhorizationRequires: false } } = to;
            if (meta.isAuhorizationRequires) {
                // El usuario no tiene una sesión valida, 
                // por lo que se redirecciona a la pantalla
                // de LOGIN
                next("/signin");
            } else { 
                if ( "signin|newPassword|mfa|forgotPassword|resetPassword|confirmEmail|confirmPhoneNumber".indexOf(to.name) >= 0) {
                    next();
                } else {
                    next("/signin");
                }
            }
        }
    }
});

export default router;